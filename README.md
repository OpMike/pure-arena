# README #
**PureArena** is a balanced class-based arena game prototype that aims to demonstrate that a massively multiplayer online role playing game (MMORPG) player versus player gameplay experience is attainable without forcing players to put in hours and hours of game time to stay competitive. This is achieved using the Unity game engine with a combination of C# scripts and the Photon networking tool for Unity 3D and following a design concept which completely ignores the time consuming components of the genre.

Unity 4.6 needed to compile!!