﻿using UnityEngine;
using System.Collections;

public class Projectile : NoProjectile {

	public float Speed { get; set; }
	public float RotaSpeed { get; set; }


	public Projectile (){
	
	}

	// Sets all ability execution relevant variables in the Player class, including caster as well as target ID and initiates the cast process
	public override void Cast()	{

			if(Caster.playerTarget != null   &&   Caster.isCasting == false  
		   &&   Ready == true   &&   Caster.distanceToTarget <=  SpellRange && Caster.lineOfSight) {
					
				Caster.setGcd = true;
				Caster.castTimeOfcurrentCast = CastTime;    
				Caster.playerOldTarget = Caster.playerTarget;
					
				PhotonView targetPhoV = (PhotonView)Caster.playerOldTarget.GetComponent("PhotonView");
				Caster.targetViewID.Clear ();
				Caster.targetViewID.Add(targetPhoV.viewID);
						
				PhotonView casterPhoV = (PhotonView)Caster.gameObject.GetComponent("PhotonView");
				Caster.casterViewID = casterPhoV.viewID;
												
				Caster.isCasting = true;
				Ready = false;
			}			
	}
}
