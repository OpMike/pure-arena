﻿using UnityEngine;
using System.Collections;

// Heal bars from https://forum.unity3d.com/threads/free-health-bar-script.193247/

public class UI : MonoBehaviour {

	public bool boolMouseOver = false;

	public Vector2 healthPos;
	public Vector2 targetHPos;
	private float barLength;
	private float targetBarLength;
	private float buffBarlength; 
	public Vector2 buffBarPos;
	public Vector2 targetBuffBarPos;

	public float playerHealthBarOffset = 1.0f;

	
	// cast bar position
	private Vector2 pos;
	private Vector2 size = new Vector2(140,20);

    public GUISkin Skin;
	public Texture2D emptyTex;
	public Texture2D fullTex;
	
	public Player pl ; // player script of the controlling player
	private Player tar ; // player script on the players target

	public Vector2 AbarPos = new Vector2(117,26); // action bar position
	public float heightAddjust = -50f;

	public bool options = false;

	public Texture2D [,] spells;
	public GUIContent[,] contentSpell;




///////
    // textures
    public Texture2D healthBackground; // back segment
    public Texture2D healthForeground; // front segment
    public GUIStyle HUDSkin = new GUIStyle(); // Styles up the health integer
  	public GUIStyle style = new GUIStyle();


    //values   
    private float healthBarWidth = 200f; //a value for creating the health bar size
       

	public int height = 20;
	public float playerHealthPercentage = 1.0f ;
	public float targetHealthPercentage = 1.0f ;

	public float gcdOffset = 0f;
	public float gcdBoxSize = 0f;

	public float distanceOffset = 0f;
	public float distanceBoxSize = 35f;



	void Awake(){
		
		// Loading the ability icons from the resources folder
		spells = new Texture2D[,]
		{
			{	(Texture2D)Resources.Load("Icon/Mage/"+"MageFireBall"), (Texture2D)Resources.Load("Icon/Mage/"+"MageFrostBolt"),
				(Texture2D)Resources.Load("Icon/Mage/"+"MageFrostBite"),(Texture2D)Resources.Load("Icon/Mage/"+"MageTidalBarrier"),
				(Texture2D)Resources.Load("Icon/Mage/"+"MageMedusasWink"),(Texture2D)Resources.Load("Icon/Mage/"+"MageTimeCut")
			},
			{	(Texture2D)Resources.Load("Icon/Priest/"+"PriestBoilingBlood"), (Texture2D)Resources.Load("Icon/Priest/"+"PriestFalseReconciliation"),
				(Texture2D)Resources.Load("Icon/Priest/"+"PriestUnholyInfusion"),(Texture2D)Resources.Load("Icon/Priest/"+"PriestBloodSacrifice"),
				(Texture2D)Resources.Load("Icon/Priest/"+"PriestDarkPact"),(Texture2D)Resources.Load("Icon/Priest/"+"PriestInnerHorror")
			},
			{	(Texture2D)Resources.Load("Icon/"+"MageFireBall"), (Texture2D)Resources.Load("Icon/"+"MageFrostBolt"),
				(Texture2D)Resources.Load("Icon/"+"MageFrostBite"),(Texture2D)Resources.Load("Icon/"+"frame-8-eerie"),
				(Texture2D)Resources.Load("Icon/"+"MageMedusasWink"),(Texture2D)Resources.Load("Icon/"+"MageTimeCut")
			}
		};

		contentSpell = new GUIContent[,]
		{
			{new GUIContent(),new GUIContent(),new GUIContent(),new GUIContent(),new GUIContent(),new GUIContent()},
			{new GUIContent(),new GUIContent(),new GUIContent(),new GUIContent(),new GUIContent(),new GUIContent()},
			{new GUIContent(),new GUIContent(),new GUIContent(),new GUIContent(),new GUIContent(),new GUIContent()}
		};

		// Assigning the icons to abilities
		// Mage spell icons
		contentSpell[0,0].image = spells[0,0];
		contentSpell[0,1].image = spells[0,1];
		contentSpell[0,2].image = spells[0,2];
		contentSpell[0,3].image = spells[0,3];
		contentSpell[0,4].image = spells[0,4];
		contentSpell[0,5].image = spells[0,5];
		
		// Priest spell icons
		contentSpell[1,0].image = spells[1,0];
		contentSpell[1,1].image = spells[1,1];
		contentSpell[1,2].image = spells[1,2];
		contentSpell[1,3].image = spells[1,3];
		contentSpell[1,4].image = spells[1,4];
		contentSpell[1,5].image = spells[1,5];
	}

	void Start () {

		pl = (Player)this.GetComponent("Player");

		// Health bar length
		barLength = (Screen.width / 4) * ((float)pl.curHealth / (float)pl.maxHealth) ;
		
		// player health bar position
		healthPos.x = ((Screen.width / 2)+(Screen.width / 6)-(barLength*0.5f)) * playerHealthBarOffset;
		healthPos.y = (Screen.height  * (float)0.7);
		
		// If a target exists display its health
		if (pl.playerTarget != null) {
			
			tar = (Player)pl.playerTarget.GetComponent ("Player");
			targetBarLength = (Screen.width / 4) * (tar.curHealth / (float)tar.maxHealth);
			targetHPos.x = (Screen.width / 2) - (Screen.width / 6) - (targetBarLength * 0.5f);
			targetHPos.y = (Screen.height * 0.7f);
		}		
		
		// Cast bar position
		pos.x = (Screen.width / 2)-70;
	}

	void Update () {
               
        playerHealthPercentage = (float)pl.curHealth / (float)pl.maxHealth;

		pl = (Player)this.GetComponent("Player");

		// Displays the cooldown time on the ability icons
		for(int i = 0; i < pl.playerClass.spell.Length; i++){

			if(pl.playerClass.spell[i].CoolDownTimer == 0)
				contentSpell[pl.classID-1,i].text = "";
			else
				contentSpell[pl.classID-1,i].text = pl.playerClass.spell[i].CoolDownTimer.ToString("0.0");
		}

		// Health bar length
		barLength = (Screen.width / 4) * (pl.curHealth / (float)pl.maxHealth) ;
		
		// player health bar position
		healthPos.x = ((Screen.width / 2) +(Screen.width / 6)-((Screen.width / 4)*0.5f)) * playerHealthBarOffset;
		healthPos.y = Screen.height  * (float)0.7;

		// If a target exists display its health
		if (pl.playerTarget != null) {
				
			tar = (Player)pl.playerTarget.GetComponent ("Player");

			//targetBarLength = (Screen.width / 4) * (float)(tar.curHealth / (float)tar.maxHealth);
			targetHealthPercentage = (float)tar.curHealth / (float)tar.maxHealth;

			targetHPos.x = (Screen.width / 2) - (Screen.width / 6) - ((Screen.width / 4) * 0.5f);
			targetHPos.y = Screen.height * (float)0.7;
		}		

		// cast bar pos
		pos.x = (Screen.width / 2)-70;
		pos.y = (Screen.height  * (float)0.8);
		
		// Buff bar length
		buffBarlength = (Screen.width / 4)  ;

		// Buff bar position
		buffBarPos.x = ((Screen.width / 2)+(Screen.width / 6)-(buffBarlength*0.5f))* playerHealthBarOffset;
		buffBarPos.y = (Screen.height  * (float)0.7)-30;

		// Target buff bar position
		targetBuffBarPos.x = (Screen.width / 2)-(Screen.width / 6)-(buffBarlength*0.5f);
		targetBuffBarPos.y = (Screen.height  * (float)0.7)-30;
	} // Update()

	
	void OnGUI(){
		
		if (pl.gcd > 0f)
            	style.normal.textColor = Color.red;
             
		// Displays the global cooldown
		GUI.Box(new Rect((Screen.width/2)-AbarPos.x + gcdOffset, (Screen.height*0.9f)-AbarPos.y+40, gcdBoxSize,20), "Global Cooldown:   " + pl.gcd.ToString("0.0"), style);
        style.normal.textColor = Color.white;

		// Displays the cast bar if the player is casting
		if (pl.isCasting== true) {

			GUI.BeginGroup (new Rect (pos.x, pos.y, size.x, size.y));
				GUI.Box (new Rect (0, 0, size.x, size.y), pl.castProgress.ToString("0.0"));
				GUI.BeginGroup (new Rect (0, 0, (size.x * pl.castProgress)/pl.castTimeOfcurrentCast, size.y));
					GUI.Box (new Rect (0, 0, size.x, size.y), pl.castProgress.ToString("0.0"));  
				GUI.EndGroup ();
			GUI.EndGroup ();
		}

        if (this.Skin != null)     {
            GUI.skin = this.Skin;
        }

		// Display death message if the player is dead
		if (pl.isDead){
			GUI.Box(new Rect((Screen.width/2)-70,Screen.height/1.5f, 140,25),"YOU HAVE DIED.", style);
			return;
		}
          
       	displayPlayerHealthAndBuffs();

       	displayTargetHealthAndBuffs();
						
       	displayActionBarAndTooltipps();
				
	}// on GUI


	void displayPlayerHealthAndBuffs(){
		
		GUI.DrawTexture (new Rect (healthPos.x, healthPos.y, healthBarWidth, height), healthBackground);       
        GUI.DrawTexture (new Rect (healthPos.x, healthPos.y, healthBarWidth * playerHealthPercentage, height), healthForeground);
       
        
        if(playerHealthPercentage <= 0.60f && playerHealthPercentage >= 0.30f){
            HUDSkin.normal.textColor = Color.yellow;
            HUDSkin.fontStyle = FontStyle.BoldAndItalic;
            HUDSkin.fontSize = 16;              
       		GUI.Label(new Rect(healthPos.x + healthBarWidth - 60, healthPos.y, 100, 50), pl.curHealth + "/" + pl.maxHealth, HUDSkin);

   
        } else if (playerHealthPercentage < 0.30f){
            HUDSkin.normal.textColor = Color.red;
            HUDSkin.fontStyle = FontStyle.BoldAndItalic;
            HUDSkin.fontSize = 16;
       		GUI.Label(new Rect(healthPos.x + healthBarWidth - 60, healthPos.y, 100, 50), pl.curHealth + "/" + pl.maxHealth, HUDSkin);
       
        } else{
            HUDSkin.normal.textColor = Color.white;
            HUDSkin.fontStyle = FontStyle.BoldAndItalic;
            HUDSkin.fontSize = 16;              
       		GUI.Label(new Rect(healthPos.x + healthBarWidth - 60, healthPos.y, 100, 50), pl.curHealth + "/" + pl.maxHealth, HUDSkin);
        }
         


		// Displays the players buffs next to its health 
		if(pl.buffs.Count!=0){
			for(int i = 0; i < pl.buffs.Count; i++){
				if(pl.buffs[i].Name == "HoTBuff")
					GUI.Label(new Rect(buffBarPos.x+i*32,buffBarPos.y, 30,30),contentSpell[1,2].image);
				if(pl.buffs[i].Name == "HealthBuff")
					GUI.Label(new Rect(buffBarPos.x+i*32,buffBarPos.y, 30,30),contentSpell[1,3].image);
				if(pl.buffs[i].Name == "ShieldBuff")
					GUI.Label(new Rect(buffBarPos.x+i*32,buffBarPos.y, 30,30),contentSpell[1,1].image);	
			}
		}

		// Displays the players debuffs next to its health 
		if(pl.debuffs.Count != 0){
			for(int i = 0; i< pl.debuffs.Count; i++){
				if (pl.debuffs[i].Name == "FireBall")
					GUI.Label(new Rect(buffBarPos.x + i * 32, buffBarPos.y + 52, 30, 30), contentSpell[0, 0].image);
				if (pl.debuffs[i].Name == "FrostBolt")
					GUI.Label(new Rect(buffBarPos.x + i * 32, buffBarPos.y + 52, 30, 30), contentSpell[0, 1].image);
				if (pl.debuffs[i].Name == "BoilingBlood")
					GUI.Label(new Rect(buffBarPos.x + i * 32, buffBarPos.y + 52, 30, 30), contentSpell[1, 0].image);
				if (pl.debuffs[i].Name == "FrostBite")
					GUI.Label(new Rect(buffBarPos.x + i * 32, buffBarPos.y + 52, 30, 30), contentSpell[0, 2].image);				
			}
		}

	}

	void displayTargetHealthAndBuffs(){

		if (pl.playerTarget != null && tar != null){

	        GUI.DrawTexture (new Rect (targetHPos.x, targetHPos.y, healthBarWidth, height), healthBackground);       
	        GUI.DrawTexture (new Rect (targetHPos.x + healthBarWidth, targetHPos.y, -healthBarWidth * targetHealthPercentage, height), healthForeground);

            if (pl.distanceToTarget > 20f)
            	style.normal.textColor = Color.red;
            else 
            	style.normal.textColor = Color.green;

			GUI.Box(new Rect((Screen.width/2)-AbarPos.x + distanceOffset, (Screen.height*0.9f)-AbarPos.y+10, distanceBoxSize,20), "Distance to target:   " + pl.distanceToTarget.ToString("0.0"), style );
			style.normal.textColor = Color.white;

           
   			  if(playerHealthPercentage <= 0.60f && playerHealthPercentage >= 0.30f){
	                HUDSkin.normal.textColor = Color.yellow;
	                HUDSkin.fontStyle = FontStyle.BoldAndItalic;
	                HUDSkin.fontSize = 16;              
	           		GUI.Label(new Rect(targetHPos.x , targetHPos.y, 100, 50), tar.curHealth + "/" + tar.maxHealth, HUDSkin);
	       
	            } else if (playerHealthPercentage < 0.30f){
	                HUDSkin.normal.textColor = Color.red;
	                HUDSkin.fontStyle = FontStyle.BoldAndItalic;
	                HUDSkin.fontSize = 16;
	           		GUI.Label(new Rect(targetHPos.x, targetHPos.y, 100, 50), tar.curHealth + "/" + tar.maxHealth, HUDSkin);
	           
	            } else {
	                HUDSkin.normal.textColor = Color.white;
	                HUDSkin.fontStyle = FontStyle.BoldAndItalic;
	                HUDSkin.fontSize = 16;              
	           		GUI.Label(new Rect(targetHPos.x , targetHPos.y, 100, 50), tar.curHealth + "/" + tar.maxHealth, HUDSkin);
	       
	        	}


			// Displays the targets buffs next to its health 
			if(tar.buffs.Count != 0){
				for(int i = 0; i< tar.buffs.Count; i++){
					if(tar.buffs[i].Name == "HoTBuff")
						GUI.Label(new Rect(targetBuffBarPos.x+i*32,targetBuffBarPos.y, 30,30),contentSpell[1,2].image);
					if(tar.buffs[i].Name == "HealthBuff")
						GUI.Label(new Rect(targetBuffBarPos.x+i*32,targetBuffBarPos.y, 30,30),contentSpell[1,3].image);
					if(tar.buffs[i].Name == "ShieldBuff")
						GUI.Label(new Rect(targetBuffBarPos.x+i*32,targetBuffBarPos.y, 30,30),contentSpell[1,1].image);

				}
			}

			// Displays the targets debuffs next to its health 
			if(tar.debuffs.Count != 0){
				for(int i = 0; i< tar.debuffs.Count; i++){
					if(tar.debuffs[i].Name == "FireBall")
						GUI.Label(new Rect(targetBuffBarPos.x+i*32,targetBuffBarPos.y+52, 30,30),contentSpell[0,0].image);
					if(tar.debuffs[i].Name == "FrostBolt")
						GUI.Label(new Rect(targetBuffBarPos.x+i*32,targetBuffBarPos.y+52, 30,30),contentSpell[0,1].image);
					if(tar.debuffs[i].Name == "BoilingBlood")
						GUI.Label(new Rect(targetBuffBarPos.x+i*32,targetBuffBarPos.y+52, 30,30),contentSpell[1,0].image);
					if(tar.debuffs[i].Name == "FrostBite")
						GUI.Label(new Rect(targetBuffBarPos.x+i*32,targetBuffBarPos.y+52, 30,30),contentSpell[0,2].image);
				}
			}
		}
	}

	void displayActionBarAndTooltipps(){
		// Displays clickable buttons with appropriate spell icons
		GUI.BeginGroup (new Rect ((Screen.width/2)-AbarPos.x, (Screen.height*0.9f)-AbarPos.y, 600, 100));

			if (GUI.Button(new Rect(10, 10, 50, 50), contentSpell[pl.classID-1,0])){
				if(pl.gcd==0)
					pl.ExecuteSpell(0);
			}
	
			if (GUI.Button(new Rect(60+5, 10, 50, 50), contentSpell[pl.classID-1,1] )){
				if(pl.gcd==0)
				pl.ExecuteSpell(1);
			}

			if (GUI.Button(new Rect(110+10, 10, 50, 50), contentSpell[pl.classID-1,2] )){
				if(pl.gcd==0)
				pl.ExecuteSpell(2);
			}

			if (GUI.Button(new Rect(160+15, 10, 50, 50), contentSpell[pl.classID-1,3] )){
				if(pl.gcd==0)
				pl.ExecuteSpell(3);
			}
			/*
				if (GUI.Button(new Rect(210+20, 10, 50, 50), contentSpell[0,4] )){
					Debug.Log("Skill 5 clicked");
				//	if(pl.gcd==0)
						//pl.ExecuteSpell(4);
				}

				if (GUI.Button(new Rect(260+25, 10, 50, 50), contentSpell[0,5] )){
					Debug.Log("Skill 6 clicked");
				//	if(pl.gcd==0)
						//pl.ExecuteSpell(5);
				}
			*/
		GUI.EndGroup ();

		// button tooltip
		for (int i = 0; i < 4; i++){
			contentSpell[pl.playerClass.classID - 1, i].tooltip = pl.playerClass.spell[i].Description;

			if (GUI.tooltip == contentSpell[pl.playerClass.classID -1, i].tooltip)  {
				boolMouseOver = true;
			}else{
				boolMouseOver = false;
			}

			if (boolMouseOver == true) {
				style.alignment = TextAnchor.MiddleCenter;
				style.wordWrap = true;
				GUI.Box(new Rect(Input.mousePosition.x - 20, Screen.height - Input.mousePosition.y - 65, 250, 60), contentSpell[pl.playerClass.classID -1, i].tooltip, style);
			}
		}
	}


}// eof