﻿using UnityEngine;
using System.Collections;

public class KeyBinds : MonoBehaviour {
	

	void Update () {

		Player player = (Player)this.GetComponent("Player");
		Transform necro = this.transform.Find("Necromancer Character");


		// If the player is moving cancles casting and prevents further casting while moving
		if (Input.GetKey (KeyCode.W)||Input.GetKey (KeyCode.A)||Input.GetKey (KeyCode.S)||Input.GetKey (KeyCode.D) || 
		    Input.GetKey (KeyCode.UpArrow)||Input.GetKey (KeyCode.DownArrow)||Input.GetKey (KeyCode.LeftArrow)||Input.GetKey (KeyCode.RightArrow)) {
			player.standStill = false;
			player.isCasting = false;
			
		} else {
			player.standStill = true;
			player.animState = "I";
			necro.localEulerAngles = new Vector3(0,0,0);
		}

		// Sets character animation state which is being broadcast to all clients
		if(Input.GetKey (KeyCode.W) )
			player.animState = "F";
		if((Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.Mouse1)))
			player.animState = "L";
		if(Input.GetKey (KeyCode.S) )
			player.animState = "B";
		if((Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.Mouse1)))
			player.animState = "R";
		if( (Input.GetKey (KeyCode.W) && Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.Mouse1)))
			player.animState = "FL";
		if((Input.GetKey (KeyCode.W) && Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.Mouse1)))
			player.animState = "FR";
		if( (Input.GetKey (KeyCode.S) && Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.Mouse1)))
			player.animState = "BL";
		if( (Input.GetKey (KeyCode.S) && Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.Mouse1)))
			player.animState = "BR";

		// Ability Execution
		// 1: Executes class ability 1
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			if(player.gcd==0)
				player.ExecuteSpell(0);
		} 

		// 2: Executes class ability 2
		if (Input.GetKeyDown(KeyCode.Alpha2)) {
			if(player.gcd==0)
				player.ExecuteSpell(1);
		}

		// 3: Executes class ability 3
		if (Input.GetKeyDown(KeyCode.Alpha3)) {
			if(player.gcd==0)
				player.ExecuteSpell(2);
		} 

		// 4: Executes class ability 4
		if (Input.GetKeyDown(KeyCode.Alpha4)) {
			if(player.gcd==0)
				player.ExecuteSpell(3);
		} 
		

		// ESC: clears current target and cancels casting
		if(Input.GetKeyDown(KeyCode.Escape)){

			if (player.playerTarget == null){
				WorkerInGame wig = (WorkerInGame)player.localScripts.GetComponent<WorkerInGame>();
				wig.showOptions = !wig.showOptions;	
			}
			player.playerTarget = null;
			player.playerOldTarget = null;

			if (GameObject.Find("TargetFriendly(Clone)") )
				Destroy(GameObject.Find("TargetFriendly(Clone)") );

			if (GameObject.Find("TargetEnemy(Clone)"))
				Destroy(GameObject.Find("TargetEnemy(Clone)") );
		}

		if(Input.GetKeyDown(KeyCode.O)){
			WorkerInGame wig = (WorkerInGame)player.localScripts.GetComponent<WorkerInGame>();
			wig.fight = true;	
		}

		// Left mouse click: sets current target
		if (Input.GetMouseButtonDown (0)) {
			player.setTargetWithMouse();
		}

		// TAB key: to iterate through hostile targets
		if (Input.GetKeyDown(KeyCode.Tab)) {
			player.setTargetWithTab(false);
		}

		// TAB key: to iterate through friendly targets
		if(Input.GetKeyDown(KeyCode.F)){
			player.setTargetWithTab(true);
		}
	}
}
