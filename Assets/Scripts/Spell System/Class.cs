﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Class   : MonoBehaviour{

	public string className;
	public string classDescription;
	public int classHealth;
	public int classID; // 1 = Ele, 2 = priest 
	public BaseAbility[] spell = new BaseAbility[4];


	public Class(int _classID) {

		classID = _classID; 

		// Sets the abilities and class attributes for the Elementalist
		if(classID == 1){ 

			className = "Elementalist";
			classDescription = "Elementalist Description";
			classHealth = 100;

			//													cd, castT,range,dmg, speed, rotaSpeed, cost
			spell[0] =  CreateProjectileSpell  ("FireBall", "", 0,  1.4f,  25f,  -6,   20,    20,       0);
			spell[1] =  CreateProjectileSpell  ("FrostBolt", "", 0, 1.4f, 25f,   -4,   20,    20,       0);
			spell[2] =  CreateNoProjectileSpell("FrostBite", "", 10,  0f, 25f,  -10,                    0);
			spell[3] =  CreateAoESpell         ("TidalBarrier", "",8, 0f, 25f,   -5,    3,    7f,       0);
			///Wink
			///TimeCut
			
			spell[0].Description = "Fireball: Hurl a ball of fire at the target: "+ spell[0].Damage + " damage, sets it on fire.";
			spell[1].Description = "Frostbolt: Shoot a frozen shard at the target: "+ spell[1].Damage + " damage, slows the target for a short period.";
			spell[2].Description = "Frostbite: Cause the air around the target to freeze: "+ spell[2].Damage+ " damage, roots the target in place for a short period.";
			spell[3].Description = "TidalBarrier: Cause a water erruption around you: "+ spell[3].Damage+ " damage, absorbs the next hostile spell for a short period.";
		}

		// Sets abilities and class attributes for the Blood Priest
		if (classID == 2){ 

			className = "Priest";
			classDescription = "Priest Description";
			classHealth = 120;

			//															cd, castT, range, dmg, cost, duration
			spell[0] =  CreateNoProjectileSpell  ("BoilingBlood", "",    0,  1.4f,  25f,   -5,  -5);
			spell[1] =  CreateShieldBuff         ("ShieldBuff", "", 	8f,   0f,  25f,     0,  -10,   4f);
			spell[2] =  CreateHoTBuff            ("HoTBuff", "",      	8f,    0,  25f,    30,  -10,   6f);
			spell[3] =  CreateHealthBuff         ("HealthBuff", "",   	 0,    0f,  25f,    30, -20,  60f);

			spell[0].Description = "Boiling Blood: Bring the targets blood to boil: " + spell[0].Damage + " damage, costs "+spell[0].Cost+" health." ;
			spell[1].Description = "False Reconciliation: Summon a protective shield around the friendly target: absorbs the next hostile spell while it lasts, costs "+spell[1].Cost+" health." ;
			spell[2].Description = "Unholy Infusion: Infuse the frendly target with unholy magic regenerating health over time: sacrifice "+spell[2].Cost+" health to heal " + spell[2].Damage + " health over time." ;
			spell[3].Description = "Blood Sacrifice: Sacrifice a large amount of your own health to temporarily increase the targets maximum health by " + spell[3].Damage + ", costs "+spell[3].Cost+" health." ;

		}
	}

	void Awake() {}

	void Start() {}

 
	public void Update () {

		// calls the Update function from the BaseAbility class for the cooldown timer of each ability
		for(int i = 0; i < spell.Length; i++){
			(spell[i]).UpdateCooldown();
		}
	}


	// Ability creation
	// Creates a projectile-less ability and sets its attributes
	public  NoProjectile CreateNoProjectileSpell(string name, string des, float coolD, float castT, float spellR, int dmg, int cost){
		
		NoProjectile spell = new NoProjectile ();
		spell.Name = name;
		spell.Description = des;
		spell.CoolDownTime = coolD;
		spell.CastTime = castT;
		spell.SpellRange = spellR;
		spell.Damage = dmg;
		spell.Cost = cost;
		return spell;		
	}

	// Creates a projectile ability and sets its attributes
	public  Projectile CreateProjectileSpell(string name, string des, float coolD, float castT, float spellR, int dmg, float speed, float rotSpeed, int cost){
		
		Projectile spell = new Projectile ();
		spell.Name = name;
		spell.Description = des;
		spell.CoolDownTime = coolD;
		spell.CastTime = castT;
		spell.SpellRange = spellR;
		spell.Damage = dmg;
		spell.RotaSpeed = rotSpeed;
		spell.Speed = speed;
		spell.Cost = cost;
		return spell;		
	}

	// Creates a Area of Effect ability and sets its attributes
	public  AoE CreateAoESpell(string name, string des, float coolD, float castT, float spellR, int dmg, int NrOTarg, float AoE_range, int cost){
		
		AoE spell = new AoE ();//(NoProjectile)CreateNoProjectileSpell( name,  des,  coolD,  castT,  spellR,  dmg,   cast_Type);
		spell.Name = name;
		spell.Description = des;
		spell.CoolDownTime = coolD;
		spell.CastTime = castT;
		spell.SpellRange = spellR;
		spell.Damage = dmg;
		spell.NrOfTargets = NrOTarg;
		spell.AoERange = AoE_range;
		spell.Cost = cost;
		return spell;		
	}

	// Creates a health buff and sets its attributes
	public  HealthBuff CreateHealthBuff(string name, string des, float coolD, float castT, float spellR, int _value, int cost, float _duration){
		
		HealthBuff spell = new HealthBuff ();//(NoProjectile)CreateNoProjectileSpell( name,  des,  coolD,  castT,  spellR,  dmg,   cast_Type);
		spell.Name = name;
		spell.Description = des;
		spell.CoolDownTime = coolD;
		spell.CastTime = castT;
		spell.SpellRange = spellR;
		spell.buffValue = _value;
		spell.Damage = _value;
		spell.Cost = cost;
		spell.buffDuration = _duration;
		return spell;		
	}
	

	// Creates a heal over time buff and sets its attributes
	public  HoTBuff CreateHoTBuff(string name, string des, float coolD, float castT, float spellR, int _value, int cost, float _duration){
		
		HoTBuff spell = new HoTBuff ();
		spell.Name = name;
		spell.Description = des;
		spell.CoolDownTime = coolD;
		spell.CastTime = castT;
		spell.SpellRange = spellR;
		spell.buffValue = _value;
		spell.Damage = _value;
		spell.Cost = cost;
		spell.buffDuration = _duration;
		return spell;		
	}

	// Creates a shield buff and sets its attributes
	public  ShieldBuff CreateShieldBuff(string name, string des, float coolD, float castT, float spellR, int _value, int cost, float _duration){
		
		ShieldBuff spell = new ShieldBuff ();
		spell.Name = name;
		spell.Description = des;
		spell.CoolDownTime = coolD;
		spell.CastTime = castT;
		spell.SpellRange = spellR;
		spell.buffValue = _value;
		spell.Cost = cost;
		spell.buffDuration = _duration;
		return spell;		
	}
} // EoF