using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;


public class Player : Photon.MonoBehaviour {
	
	// Local networking scripts
	public GameObject localScripts;
   
	// Player variables
	public string playerName;
	public Class playerClass;
	public int maxHealth;
	public int curHealth;
	public bool isDead = false;
	public bool isGrounded = true;
	public GameObject playerTarget; 
	public GameObject playerOldTarget; // used when target has changed while casting
	public List<GameObject> friendlyTargets = new List<GameObject>();
	public List<GameObject> hostileTargets = new List<GameObject>();
	public List<int> targetViewID = new List<int>();	
	public int casterViewID; 
	public int curSpellID; 
	public bool mine;	
	public float distanceToTarget ;
	public int team = 0;
	public int classID;

	// Cast variables
	public bool isCasting = false;
	public float castTimeOfcurrentCast; 
	public float castProgress;
	public bool standStill;

	// Line of Sight
	public bool lineOfSight = false;
	public LayerMask envirLayer;

	// Global cooldown variables
	public float gcd; 
	public bool setGcd;

	// Buffs & Debuffs variables
	public List<Buff> buffs = new List<Buff>();	
	public List<Buff> debuffs = new List<Buff>();

	// Animation variables
	public string animState;
	public Animation anim;
	public AnimationClip idle;
	public AnimationClip forward;
	public AnimationClip backward;
	public AnimationClip left;
	public AnimationClip right;
	public AnimationClip leftForward;
	public AnimationClip leftBackward;
	public AnimationClip rightForward;
	public AnimationClip rightBackward;
	public AnimationClip jump;
	public AnimationClip land;
	public AnimationClip die;
	public AnimationClip cast;
	public AnimationClip gethit;
	public AnimationClip gethit2;
	

	public Player(){
	}

	void Awake(){

		// gets the TeamID from the networking script to assign the player to the appropriate team
		localScripts = GameObject.Find("scripts");
		team = ((WorkerInGame)localScripts.GetComponent<WorkerInGame>()).TeamID;

		// sets the appropriate player class, abilities and class health
		playerClass = new Class(classID);
		maxHealth = curHealth = playerClass.classHealth;
	}

	// broadcasts local variables of the controlling player to the players 'copies' on the other clients
	// sends if the client is controlled locally, receives if not
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){

		if (stream.isWriting){
			if(info.photonView.isMine)
				mine = info.photonView.isMine;            
			stream.SendNext(maxHealth); 
			stream.SendNext(curHealth);
			stream.SendNext(team);
			stream.SendNext(animState);
			stream.SendNext(isDead);
			stream.SendNext(castProgress);
		}
		else{
			mine = false;
			maxHealth = (int)stream.ReceiveNext();
			curHealth = (int)stream.ReceiveNext();
			team = (int)stream.ReceiveNext();
			animState = (string)stream.ReceiveNext();
			isDead = (bool)stream.ReceiveNext();
			castProgress = (float)stream.ReceiveNext();
		}
	}
	

	void Update () {
		setLineOfSight();
		updateAnimation();
		playerClass.Update(); // Calls the playerClass's Update() function, needed for the cooldown counters for each ability within
		updateBuffs();
		setTeamCollisionLayer();
		updateCasting();
		spellExecution();
		updateRangeToTarget();
	}

 
	///RPCs
	[RPC] // Projectile RPC
	public void Proj(string prefabName,int dmg, int casterID,int[] tarID,int _team){

		GameObject caster = new GameObject();
		GameObject target = new GameObject();
		Destroy (caster);
		Destroy (target);
		bool targetPresent = false;
		bool casterPresent = false;

		// Checks caster and target IDs of all current players to find targer and caster
		foreach(GameObject gos in GameObject.FindGameObjectsWithTag("Player"))	{

			if(gos.GetComponent("PhotonView")!= null){

				PhotonView pho = (PhotonView)gos.GetComponent("PhotonView");

				if(int.Parse( pho.viewID.ToString()) == casterID){

					caster = gos;
					targetPresent = true;
				}

				if(int.Parse( pho.viewID.ToString()) == tarID[0]){

					target = gos;
					casterPresent = true;
				}  
			}
		}


		// If caster and target ID have been found instantiate prefab at correct player and assign target to instantiated projectile
		if(casterPresent && targetPresent){
				
				Transform spellSpawn = (Transform)caster.gameObject.transform.Find("SpellSpawn").transform ;
				
				// Instantiates the ability prefab locally
				GameObject gol = Resources.Load("Prefabs/"+prefabName, typeof(GameObject)) as GameObject;
				GameObject go = (GameObject)Instantiate (gol, spellSpawn.position, caster.transform.rotation) ;
				

				// Assigns the Projectile GameObject to the right layer
				if(_team == 1)
					go.layer = LayerMask.NameToLayer("Team1");
				if(_team == 2)
					go.layer = LayerMask.NameToLayer("Team2");

			
				// Sets the spell values on the instantiated prefab
				CollisionDestroy colliScript = (CollisionDestroy)go.GetComponent("CollisionDestroy");
				colliScript.target = target; 
				colliScript.destructionTime = 0f;
				colliScript.damage = dmg;
				colliScript.prefabName = prefabName; 
				//colliScript.moveSpeed = speed;
				//colliScript.rotationSpeed = rotaSpeed;
			}
	} // Projectile RPC
	
	[RPC] // NoProjectile RPC
	public void NoProj(string prefabName,int dmg, int casterID,int[] tarID,int _team){
		
		GameObject caster = new GameObject();
		GameObject target = new GameObject();
		Destroy (caster);
		Destroy (target);
		bool targetPresent = false;
		bool casterPresent = false;
		
		// Checks caster and target IDs of all current players to find targer and caster
		foreach(GameObject gos in GameObject.FindGameObjectsWithTag("Player"))	{
			
			if(gos.GetComponent("PhotonView")!= null){
				
				PhotonView pho = (PhotonView)gos.GetComponent("PhotonView");
				
				if(int.Parse( pho.viewID.ToString()) == casterID){
					
					caster = gos;
					targetPresent = true;
				}
				
				if(int.Parse( pho.viewID.ToString()) == tarID[0]){
					
					target = gos;
					casterPresent = true;
				}  
				
			}
		}
		
		
		// If caster and target ID have been found instantiate prefab at correct player and assign target to instantiated projectile
		if(casterPresent && targetPresent){

			// Instantiates the ability prefab locally
			GameObject gol = Resources.Load("Prefabs/"+prefabName, typeof(GameObject)) as GameObject;
			GameObject go = (GameObject)Instantiate (gol, target.transform.position, caster.transform.rotation) ;

			// Assigns the Projectile GameObject to the right layer
			if(_team == 1)
				go.layer = LayerMask.NameToLayer("Team1");
			if(_team == 2)
				go.layer = LayerMask.NameToLayer("Team2");

			
			// Sets the spell values on the instantiated prefab
			CollisionDestroy colliScript = (CollisionDestroy)go.GetComponent("CollisionDestroy");
			colliScript.target = target; 
			colliScript.destructionTime = 0f;
			colliScript.damage = dmg;
			colliScript.prefabName = prefabName; 
		}
	} // NoProjectile RPC
	
	[RPC] // Area of Effect RPC
	public void AreaOfEffect(string prefabName,int dmg, int casterID, int[] tarID,int _team){
		
		GameObject caster = new GameObject();
		//Destroy (caster);
		bool casterPresent = false;
		
			// Checks all player IDs to find the caster
			foreach(GameObject gos in GameObject.FindGameObjectsWithTag("Player"))	{
				
					PhotonView pho = (PhotonView)gos.GetComponent("PhotonView");

						if(pho != null)
							if( casterPresent == false  &&  ( pho.viewID == casterID)){
								
								caster = gos;
								casterPresent = true;
							}
									
			}
		
		// If the caster has been found instantiate prefab at  caster position
		if(casterPresent){

			GameObject gol = Resources.Load("Prefabs/"+prefabName, typeof(GameObject)) as GameObject;
			GameObject go = (GameObject)Instantiate (gol, caster.transform.position, caster.transform.rotation) ;


			// Assigns the Projectile GameObject to the right layer
			if(_team == 1)
				go.layer = LayerMask.NameToLayer("Team1");
			if(_team == 2)
				go.layer = LayerMask.NameToLayer("Team2");
			
			
			// Sets the spell values on the instantiated prefab
			CollisionDestroy colliScript = (CollisionDestroy)go.GetComponent("CollisionDestroy");
			colliScript.damage = dmg;		
			colliScript.destructionTime = 1f;
			colliScript.prefabName = prefabName; 

		}		
	} // Area of Effect RPC
	
	[RPC] // Health Buff RPC
	public void HpBuff(string name,int _value,float _duration,float coolD , int casterID, int[] tarID){

		HealthBuff hpBuff = new HealthBuff();
		hpBuff.Name = name;
		hpBuff.buffValue = _value;
		hpBuff.CoolDownTime = coolD;
		hpBuff.buffDuration = hpBuff.remainingDuration = _duration;
		
		GameObject caster = new GameObject();
		List<GameObject> targets = new List<GameObject>();	
		
		Destroy (caster);

		// Checks all player IDs to find the caster and target
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Player"))	{
			
			if(go.GetComponent("PhotonView")!= null){

				PhotonView pho = (PhotonView)go.GetComponent("PhotonView");
							
				if(  (int.Parse( pho.viewID.ToString()) == casterID))
							caster = go;

				if(  (int.Parse( pho.viewID.ToString()) == tarID[0]))
							targets.Add(go);
			}
		}

		// Executes the Addbuff function
		AddBuff(hpBuff, targets[0]);
		
	} // Health Buff RPC
								  
	[RPC] // Heal over Time Buff RPC
	public void HotBuff(string name,int _value,float _duration, float coolD, int casterID, int[] tarID){

		HoTBuff hot = new HoTBuff();
		hot.Name = name;
		hot.buffValue = _value;
		hot.CoolDownTime = coolD;
		hot.buffDuration = hot.remainingDuration = _duration;

		GameObject caster = new GameObject();
		List<GameObject> targets = new List<GameObject>();	
		Destroy (caster);
		
		// Checks all player IDs to find the caster and target
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Player"))	{
			
			if(go.GetComponent("PhotonView")!= null){
				
				PhotonView pho = (PhotonView)go.GetComponent("PhotonView");
				
				if(  (int.Parse( pho.viewID.ToString()) == casterID)){
					
					caster = go;
				}
				
				if(  (int.Parse( pho.viewID.ToString()) == tarID[0])){
					
					targets.Add(go);
				}
			}
		}

		// Executes the Addbuff function to add the buff
		AddBuff(hot,targets[0]);
		
	} // Heal over Time Buff RPC
	
	[RPC] // Shield Buff RPC
	public void ShieldBuff(string name,int _value, float _duration,float coolD , int casterID, int[] tarID){

		ShieldBuff shield = new ShieldBuff();
		shield.Name = name;
		shield.buffValue = _value;
		shield.CoolDownTime = coolD;
		shield.buffDuration = shield.remainingDuration = _duration;

		GameObject caster = new GameObject();
		List<GameObject> targets = new List<GameObject>();	
		Destroy (caster);
		
		// Executes the Addbuff function to add the buff
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Player"))	{
			
			if(go.GetComponent("PhotonView")!= null){
				
				PhotonView pho = (PhotonView)go.GetComponent("PhotonView");
				
				if(  (int.Parse( pho.viewID.ToString()) == casterID))
								caster = go;
								
				if(  (int.Parse( pho.viewID.ToString()) == tarID[0]))					
					targets.Add(go);
			}
		}
		
		// Executes the Addbuff function to add the buff
		AddBuff(shield,targets[0]);
		
	} // Shield Buff RPC
	
	// Adds a Buff to the targets buff array after checking if the buff type is already present
	public void AddBuff(Buff buff, GameObject target){

		// Gets the type of the buff to check it against the targets buffs array
		List<Buff> bs = ((Player)target.GetComponent("Player")).buffs;	
		
		for(int i = bs.Count - 1; i > -1; i--){
			if(bs[i].Name == buff.Name){

				if(bs[i].Name == "ShieldBuff")
					Destroy(target.transform.Find("ShieldBuff(Clone)").gameObject);

				bs.RemoveAt(i);
			}			
		}

		bs.Add(buff);

		if(buff.Name == "ShieldBuff"){

			GameObject go = (GameObject)Instantiate (Resources.Load("Prefabs/"+"ShieldBuff", typeof(GameObject))) ;
			go.transform.position = target.transform.position;

			go.transform.parent = target.transform;

		}

	} // AddBuff()
	
	// Adds a Debuff to the targets buff array after checking if the buff type is not present
	public void AddDebuff(Buff debuff, GameObject target){


		List<Buff> dbs = ((Player)target.GetComponent("Player")).debuffs;	

		for(int i = dbs.Count - 1; i > -1; i--){
			if(dbs[i].Name == debuff.Name)
				dbs.RemoveAt(i);
				
		}

		dbs.Add(debuff);

		
		
	} // AddDebuff()
	
	// Executes the appropriate ability depending on player class
	public void ExecuteSpell(int c){

		if(lineOfSight){

			Player playTar = new Player();
			if(playerTarget != null)
			playTar = (Player)playerTarget.GetComponent("Player");

			// Executes the Cast function within the ability which is stored in the Class class
			if((playerClass.spell[c] is Buff  && playTar.team == this.team)   ||   (playerClass.spell[c] is AoE  )  
					||  (playerClass.spell[c] is NoProjectile  && playTar.team != this.team)){

				playerClass.spell[c].Caster = this;
				playerClass.spell[c].Cast();
				curSpellID = c;
			}

		}

	} // ExecuteSpell()
	
	// Addjusts the players health by X amount
	public void addjustCurrentHealth(int adj){
		//animState = "HIT";

		curHealth += adj;
		
		if (curHealth < 0)
			curHealth = 0;
		
		if(curHealth > maxHealth)
			curHealth = maxHealth;
		
		if(maxHealth < 1)
			maxHealth = 1;	
	}

	public void setTargetWithTab(bool friendly){

 			friendlyTargets.Clear();
			hostileTargets.Clear();

			WorkerInGame wig = (WorkerInGame)localScripts.GetComponent("WorkerInGame");

			foreach (GameObject target in wig.allPlayers){
				if((Player)target.GetComponent("Player")){
					Player tarPlayer = (Player)target.GetComponent("Player");

					if (tarPlayer.team == this.team){
						friendlyTargets.Add(target);
					}

					if (tarPlayer.team != this.team){
						hostileTargets.Add(target);
					}
				}
			}

		if (friendly)
			setTargetByAllignment(friendlyTargets);
		else 
			setTargetByAllignment(hostileTargets);
		
		addTargetVisuals();
	}

	private void setTargetByAllignment(List<GameObject> trgts){
		if(trgts != null && trgts.Count != 0){
			if (playerTarget == null){

				playerTarget = trgts[0];

				if(playerOldTarget == null)
					playerOldTarget = trgts[0];

			} else if (playerTarget == trgts[trgts.Count-1]){

				playerOldTarget = playerTarget;
				playerTarget = trgts[0];
			} else {
				for(int i = 0; i < trgts.Count; i++){

					if (playerTarget == trgts[i]){

						playerOldTarget = playerTarget;
						playerTarget = trgts[i+1];
						break;
					} else {
						playerOldTarget = playerTarget;
						playerTarget = trgts[0];
					}

				}
			}
		}
	}
	
	// Sets the player target, which is used for ability execution
	public void setTargetWithMouse(){

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		// Casts a ray from the centre of the screen towards the current mouse position
		if (Physics.Raycast (ray.origin, ray.direction, out hit, 1500f)) {

			// If the colliding game object is a player, set that player as the target GameObject
			if(hit.collider.gameObject.GetComponent("Player") /*&& hit.collider.gameObject.GetComponent("Player")  !=  this.gameObject.GetComponent("Player")*/){
				playerTarget = hit.collider.gameObject;

			if(playerOldTarget == null)
				playerOldTarget = hit.collider.gameObject;
			}
		}
		addTargetVisuals();
	} // set Target()

	private void addTargetVisuals(){
		// Target Visualisation
		// Remove the target marker from the scene
		if (GameObject.Find("TargetFriendly(Clone)") )
			Destroy(GameObject.Find("TargetFriendly(Clone)") );

		if (GameObject.Find("TargetEnemy(Clone)"))
			Destroy(GameObject.Find("TargetEnemy(Clone)") );

		// If the player has a target instantiate a target marker to visualy distiguish the target from other players
		if(playerTarget != null ){

			Player tarPlayer = (Player)playerTarget.GetComponent("Player");

			if (tarPlayer.team == this.team){ 
				GameObject go = (GameObject)Instantiate (Resources.Load("Prefabs/"+"TargetFriendly", typeof(GameObject)), playerTarget.transform.position, playerTarget.transform.rotation) ;
				go.transform.parent = playerTarget.transform;
			}
			else{ 
				GameObject go = (GameObject)Instantiate (Resources.Load("Prefabs/"+"TargetEnemy", typeof(GameObject)), playerTarget.transform.position, playerTarget.transform.rotation) ;
				go.transform.parent = playerTarget.transform;
			}

			// Stores range to current target 
			distanceToTarget = Vector3.Distance(playerTarget.transform.position, transform.position);
		}
	}


	private void updateCasting(){
			// Prevents casting if the player has no target for all ability types except AoE
			if (playerTarget == null && !(playerClass.spell[curSpellID] is AoE))
				isCasting = false;

			// Sets the global cooldown 
			if (setGcd == true){
				gcd = 1.5f;
				setGcd = false;
			}

			// Global cooldown timer
			if (gcd <= 0){
				gcd = 0;
			}
			else{
				gcd -= Time.deltaTime;
			}

			// Does cast progress calculations if player is casting
			if (isCasting == true) {
				if (castProgress < castTimeOfcurrentCast) { 
					castProgress += Time.deltaTime;
				}
				if (gcd == 0){
					setGcd = true;
				}
			}
			else{
				castProgress = 0;
			}
	}

	private void spellExecution() {

		// If the cast time of the current spell is reached, execute approproate RPC depending on ability type
		if ((castProgress >= castTimeOfcurrentCast && isCasting == true)){
			isCasting = false;
			castProgress = 0;

			if (lineOfSight || playerClass.spell[curSpellID] is AoE) {
				if (playerClass.spell[curSpellID] is Projectile) {
					GetComponent<PhotonView>().RPC(
						"Proj",
						PhotonTargets.AllViaServer,
						new object[]{
							playerClass.spell[curSpellID].Name, playerClass.spell[curSpellID].Damage, casterViewID, targetViewID.ToArray(),team
						}
					);
				}
				else
				if (playerClass.spell[curSpellID] is AoE){
					GetComponent<PhotonView>().RPC(
						"AreaOfEffect",
						PhotonTargets.AllViaServer,
						new object[]{
							playerClass.spell[curSpellID].Name, playerClass.spell[curSpellID].Damage, casterViewID, targetViewID.ToArray(),team
						}
					);
				}
				else
				if (playerClass.spell[curSpellID] is NoProjectile){
					GetComponent<PhotonView>().RPC(
					"NoProj",
					PhotonTargets.AllViaServer,
					new object[]{
						playerClass.spell[curSpellID].Name, playerClass.spell[curSpellID].Damage, casterViewID, targetViewID.ToArray(),team
					}
					);
				}
				else
				if (playerClass.spell[curSpellID] is HealthBuff){
					GetComponent<PhotonView>().RPC(
						"HpBuff",
						PhotonTargets.AllViaServer,
						new object[]{
							playerClass.spell[curSpellID].Name,   ((HealthBuff)playerClass.spell[curSpellID]).buffValue,
							((HealthBuff)playerClass.spell[curSpellID]).buffDuration,((HealthBuff)playerClass.spell[curSpellID]).CoolDownTime,
							casterViewID,   targetViewID.ToArray()
						}
					);
				}
				else
				if (playerClass.spell[curSpellID] is HoTBuff){
					GetComponent<PhotonView>().RPC(
						"HotBuff",
						PhotonTargets.AllViaServer,
						new object[]{
						playerClass.spell[curSpellID].Name,   ((HoTBuff)playerClass.spell[curSpellID]).buffValue,
							((HoTBuff)playerClass.spell[curSpellID]).buffDuration,((HoTBuff)playerClass.spell[curSpellID]).CoolDownTime,    casterViewID,   targetViewID.ToArray()
						}
					);
				}
				else
				if (playerClass.spell[curSpellID] is ShieldBuff){
					GetComponent<PhotonView>().RPC(
						"ShieldBuff",
						PhotonTargets.AllViaServer,
						new object[]{
							playerClass.spell[curSpellID].Name,   ((ShieldBuff)playerClass.spell[curSpellID]).buffValue,
							((ShieldBuff)playerClass.spell[curSpellID]).buffDuration,((ShieldBuff)playerClass.spell[curSpellID]).CoolDownTime,     casterViewID,   targetViewID.ToArray()
						}
					);
				}
			} /// if LoS
		} 
	}

	private void updateAnimation(){

		// if the player's health is 0 or less the death state variable is set to true
		if (curHealth <= 0)
			isDead = true;
		// sets variable to true if the player is on the ground
		isGrounded = ((ThirdPersonControllerNET)this.GetComponent<ThirdPersonControllerNET>()).Grounded;

		// gets animation component
		if (!anim)
			anim = GetComponentInChildren<Animation>();

		// if the player is dead and on the ground set animation script accordingly and turn of movement and key bindings script.
		if (isDead && isGrounded)
		{
			animState = "DEAD";
			((ThirdPersonControllerNET)this.GetComponent<ThirdPersonControllerNET>()).SetIsRemotePlayer(true);
			((KeyBinds)this.GetComponent<KeyBinds>()).enabled = false;
			//KeyBinds kb = GetComponent<KeyBinds>();
		}
		else if (mine)
		{
			((ThirdPersonControllerNET)this.GetComponent<ThirdPersonControllerNET>()).SetIsRemotePlayer(false);
			((KeyBinds)this.GetComponent<KeyBinds>()).enabled = true;
		}

		// if the player is  jumping set animation state acordingly
		if (!isGrounded)
			animState = "J";

		// if the player is  casting set animation state acordingly
		if (castProgress != 0)
			animState = "CAST";

		// gets the transform of the class model on the player GameObject
		Transform necro = this.transform.Find("Necromancer Character");

		// sets the animation speed for all animations to 1
		foreach (AnimationState an in anim)
			an.speed = 1f;


		// Animation States
		// Executes appropriate animations depending on the animation state

		switch (animState){
			case "DEAD":
				anim.wrapMode = WrapMode.ClampForever;
				anim.CrossFade(die.name);
				break;
			case "CAST":
				if (classID == 1) {
					foreach (AnimationState an in anim)
						an.speed = 0.4f;
				}
				anim.CrossFade(cast.name);
				break;
			case "J":
				anim.CrossFade(jump.name);
				break;
			case "I":
				anim.CrossFade(idle.name);
				necro.localEulerAngles = new Vector3(0, 0, 0);

				break;
			case "F":
				anim.CrossFade(forward.name);
				necro.localEulerAngles = new Vector3(0, 0, 0);
				break;
			case "FL":
				anim.CrossFade(forward.name);
				necro.localEulerAngles = new Vector3(0, -45, 0);
				break;
			case "FR":
				anim.CrossFade(forward.name);
				necro.localEulerAngles = new Vector3(0, 45, 0);
				break;	
			case "L":
				anim.CrossFade(forward.name);
				necro.localEulerAngles = new Vector3(0, -90, 0);
				break;
			case "R":
				anim.CrossFade(forward.name);
				necro.localEulerAngles = new Vector3(0, 90, 0);
				break;	
			case "B":
				anim.CrossFade(forward.name);
				necro.localEulerAngles = new Vector3(0, -180, 0);
				break;	
			case "BL":
				anim.CrossFade(forward.name);
				necro.localEulerAngles = new Vector3(0, -135, 0);							
				break;
			case "BR":
				anim.CrossFade(forward.name);
				necro.localEulerAngles = new Vector3(0, 135, 0);							
				break;				
			default:
				anim.wrapMode = WrapMode.Loop;
				break;
		}

	}

	private void updateBuffs(){
		// Updates and removes buffs
		if (buffs.Count != 0){
			for (int i = buffs.Count - 1; i > -1; i--){
				buffs[i].Caster = this;
				buffs[i].UpdateBuff();

				if (buffs[i].remainingDuration <= 0f){
					buffs.RemoveAt(i);
				}
			}
		}

		// Updates and removes debuffs
		if (debuffs.Count != 0){
			for (int i = debuffs.Count - 1; i > -1; i--){
				debuffs[i].Caster = this;
				debuffs[i].UpdateBuff();

				if (debuffs[i].remainingDuration < 0f){
					debuffs.RemoveAt(i);
				}
			}
		}

		// Sets the current health to maximum health if the current health is larger 
		if (curHealth > maxHealth) {
			curHealth = maxHealth;
		}
	}

	private void setTeamCollisionLayer(){
		// If the player GameObject is controlled locally all player GameObjects are assigned to a physics layer which is needed for collision
		if (mine == true){
			foreach (GameObject go in GameObject.FindGameObjectsWithTag("Player")){
				if (((Player)go.GetComponent("Player")).team == 1){
					go.layer = LayerMask.NameToLayer("Team1");
				}

				if (((Player)go.GetComponent("Player")).team == 2){
					go.layer = LayerMask.NameToLayer("Team2");
				}
			}
		}
	}

	private void updateRangeToTarget(){
		// Stores range to current target 
		if (playerTarget != null)
			distanceToTarget = Vector3.Distance(playerTarget.transform.position, transform.position);
	}

	private void setLineOfSight(){
		if (playerOldTarget != null)
		{
			RaycastHit hitt;

			if (Physics.Linecast(this.transform.position, playerOldTarget.transform.position, out hitt, envirLayer))
			{ // 0 = Default layer
				lineOfSight = false;
				//Debug.Log(hitt.collider.gameObject.name);
			}
			else
			{
				lineOfSight = true;
			}

			Debug.DrawLine(this.transform.position, playerOldTarget.transform.position, Color.red);
			//Debug.Log(lineOfSight);
		}

		// Visualises the player targeting, not needed for gameplay  
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		Debug.DrawRay(ray.origin, ray.direction * 300, Color.yellow);

	}

} /// PLAYER EoF
