﻿using UnityEngine;
using System.Collections;
using System.Linq;


public class ShieldBuff : Buff {

	
	public ShieldBuff (){
		
	}

	// Checks if the shield has been absorbed and keeps track of the buff duration
	public override  void UpdateBuff () {

		if(remainingDuration > 0){
			remainingDuration -= Time.deltaTime;
		}

		if(Caster.gameObject.transform.Find("ShieldBuff(Clone)") != null){
			if(remainingDuration <= 0){
				Destroy(Caster.gameObject.transform.Find("ShieldBuff(Clone)").gameObject);

			}
		}
		else
			remainingDuration = 0;

	}


	// Sets all ability execution relevant variables in the Player class, including caster as well as target ID and initiates the casting process
	public override void Cast()	{

		if(Caster.playerTarget != null   &&   Caster.isCasting == false &&  
		   				Ready == true  &&   Caster.distanceToTarget <= SpellRange && Caster.lineOfSight)  {

			Caster.setGcd = true;
			Caster.castTimeOfcurrentCast = CastTime;    
			Caster.playerOldTarget = Caster.playerTarget;
			
			PhotonView targetPhoV = (PhotonView)Caster.playerOldTarget.GetComponent("PhotonView");
			Caster.targetViewID.Clear ();
			Caster.targetViewID.Add(targetPhoV.viewID);

			PhotonView casterPhV = (PhotonView)Caster.gameObject.GetComponent("PhotonView");
			Caster.casterViewID = casterPhV.viewID;

			Caster.isCasting = true;
			Ready = false;

			if(Caster.curHealth > Caster.maxHealth*0.2)
				Caster.addjustCurrentHealth(Cost);
		}
	}
}
