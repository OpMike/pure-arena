﻿using UnityEngine;
using System.Collections;

public class Slow : Buff {

	public float player_speed;

	public Slow (){
		
	}

	// Applies the debuff effect to the player and tracks the debuff duration
	public override  void UpdateBuff () {	


		if(remainingDuration > 0){
			remainingDuration -= Time.deltaTime;
			((ThirdPersonControllerNET)Caster.gameObject.GetComponent<ThirdPersonControllerNET>()).Speed = player_speed * (buffValue/100f);
		}

		if(remainingDuration <= 0){
			((ThirdPersonControllerNET)Caster.gameObject.GetComponent<ThirdPersonControllerNET>()).Speed = player_speed  ;

		}
	}
}
