﻿using UnityEngine;
using System.Collections;
using System.Linq;


public class HoTBuff : Buff {


	public float tickTimer;

	public HoTBuff (){
	
	}

	// Applies the buff effect to the player and tracks the buff duration
	public override  void UpdateBuff () {	

		if(remainingDuration > 0){

			remainingDuration -= Time.deltaTime;
			tickTimer += Time.deltaTime;

			if(tickTimer > 1.0f){

				tickTimer -= 1.0f;
				Caster.addjustCurrentHealth((int)(buffValue/buffDuration));
			}
		} 
	}


	// Sets all relevant variables including caster as well as target ID and initiates the cast process
	public override void Cast()	{

		if(Caster.playerTarget != null   &&   Caster.isCasting == false  &&   
		   				Ready == true &&   Caster.distanceToTarget <= SpellRange && Caster.lineOfSight )  {

			Caster.setGcd = true;
			Caster.castTimeOfcurrentCast = CastTime;     
			Caster.playerOldTarget = Caster.playerTarget;
			
			PhotonView targetPhoV = (PhotonView)Caster.playerOldTarget.GetComponent("PhotonView");
			Caster.targetViewID.Clear ();
			Caster.targetViewID.Add(targetPhoV.viewID);
			 
			PhotonView casterPhV = (PhotonView)Caster.gameObject.GetComponent("PhotonView");
			Caster.casterViewID = casterPhV.viewID;

			Caster.isCasting = true;
			Ready = false;

			if(Caster.curHealth > Caster.maxHealth*0.2)
				Caster.addjustCurrentHealth(Cost);

		}
	}
}
