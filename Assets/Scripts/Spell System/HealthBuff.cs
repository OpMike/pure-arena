﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class HealthBuff : Buff {


	public HealthBuff (){

	}


	// Applies the buff effect to the player and tracks the buff duration
	public override  void UpdateBuff () {	
		remainingDuration -= Time.deltaTime;

		if(remainingDuration > 0)
			Caster.maxHealth = Caster.playerClass.classHealth + buffValue;

		else 
			Caster.maxHealth = Caster.playerClass.classHealth;

	}

	// Sets all ability execution relevant variables in the Player class, including caster as well as target ID and initiates the casting process
	public override void Cast()	{

		if(Caster.playerTarget != null   &&   Caster.isCasting == false  &&   Ready == true &&  
		   									Caster.distanceToTarget <= SpellRange && Caster.lineOfSight )  {
			
			Caster.setGcd = true;
			Caster.castTimeOfcurrentCast = CastTime;     
			Caster.playerOldTarget = Caster.playerTarget;
			
			PhotonView targetPhoV = (PhotonView)Caster.playerOldTarget.GetComponent("PhotonView");
			Caster.targetViewID.Clear ();
			Caster.targetViewID.Add(targetPhoV.viewID);

			PhotonView casterPhV = (PhotonView)Caster.gameObject.GetComponent("PhotonView");
			Caster.casterViewID = casterPhV.viewID;

			Caster.isCasting = true;
			Ready = false;

			if(Caster.curHealth > Caster.maxHealth*0.2){

				Player targetPlayer = (Player)Caster.playerOldTarget.GetComponent("Player");

				if (targetPlayer.buffs.Contains(this))
					Caster.addjustCurrentHealth(Cost/4);
				else
					Caster.addjustCurrentHealth(Cost);


			}
		}
	}
}
