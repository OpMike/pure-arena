﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class BaseAbility : MonoBehaviour,  IBaseAbility {

	public string Name {get; set;}
	public string Description {get;set;}
	public float CoolDownTime {get;set;}
	public float CoolDownTimer {get; set;}
	public bool Ready {get;set;}
	public Player Caster {get;set;}
	public int Damage { get; set; }
	public int Cost { get; set; }
	public float SpellRange { get; set; }
	public float CastTime { get; set;}


	public BaseAbility( ) {		
		Ready = true;
	}


	// Keeps track of the cool down time
	public  void UpdateCooldown () {	
		
		if(Ready == false && CoolDownTimer == 0)
			CoolDownTimer = CoolDownTime;

		if (CoolDownTimer > 0 && Ready == false)
			CoolDownTimer -= Time.deltaTime;

		if(CoolDownTimer < 0)
			CoolDownTimer = 0;

		if(CoolDownTimer==0)
			Ready = true;
	}


	public virtual void Cast()	{

	}

}
