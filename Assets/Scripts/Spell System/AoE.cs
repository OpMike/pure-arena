﻿using UnityEngine;
using System.Collections;

public class AoE : NoProjectile {


	public int NrOfTargets { get; set; }
	public float AoERange { get; set; }


	// Constructor
	public AoE (){
	
	}

	// Sets all execution relevant variables in the Player class, including caster as well as target ID and initiates the casting process
	public override void Cast()	{

		if( Caster.isCasting == false   &&   Ready == true ) {
			
			Caster.setGcd = true;
			Caster.castTimeOfcurrentCast = CastTime;   

			if(Caster.playerTarget != null)
				Caster.playerOldTarget = Caster.playerTarget;

			PhotonView casterPhV = (PhotonView)Caster.gameObject.GetComponent("PhotonView");
			Caster.casterViewID = casterPhV.viewID;

			Caster.isCasting = true;
			Ready = false;
		}
	}
}
