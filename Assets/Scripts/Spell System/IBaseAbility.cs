﻿using UnityEngine;
using System.Collections;

public interface IBaseAbility {

	string Name {get; set;}
	string Description {get;set;}
	float CoolDownTime {get;set;}
	float CoolDownTimer {get; set;}
	int Damage { get; set; }
	int Cost { get; set; }

	bool Ready {get;set;}
	Player Caster {get;set;}

	float SpellRange { get; set; }
	float CastTime { get; set;}
	
	void UpdateCooldown();

	void Cast();
	//BaseAbility CreateSpell(string name, string des, float coolD,  string cast_Type);

		
}
