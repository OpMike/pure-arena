// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkerMenu.cs" company="Exit Games GmbH">
//   Part of: Photon Unity Networking
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class WorkerMenu : MonoBehaviour
{
    public GUISkin Skin;
    public RectTransform imagePos;



    public float menuAnimSpeed = 1f;
    public float animThreshold = 0.05f;


    public bool showMenu = true;
    public Vector2 startButtonPos1 = new Vector2(0,0);
    public Vector2 startButtonPos2 = new Vector2(0,0);
    public Vector2 startButtonCurPos = new Vector2(0,0);
    public float startButtonDirX = 1f;
    public float startButtonDirY = 1f;


    public Vector2 exitButtonPos1 = new Vector2(0,0);
    public Vector2 exitButtonPos2 = new Vector2(0,0);
    public Vector2 exitButtonCurPos = new Vector2(0,0);
    public float exitButtonDirX = 1f;
    public float exitButtonDirY = 1f;   

    public bool showServerMenu = true;
    public Vector2 servMenuPos1 = new Vector2(0,0);
    public Vector2 servMenuPos2 = new Vector2(0,0);
    public Vector2 curServMenuPos = new Vector2(0,0);
    public float servMenuDirX = 1f;
    public float servMenuDirY = 1f;
    public Vector2 WidthAndHeight = new Vector2(600,350);
    public Vector2 servOffset = new Vector2(1,1);

    public Vector2 titlePos1 = new Vector2(0,0);
    public Vector2 titlePos2 = new Vector2(0,0);
    public Vector2 curTitlePos = new Vector2(0,0);  
    public Vector2 titleOffset = new Vector2(1,1);
  
    public Vector2 backPos1 = new Vector2(0,0);
    public Vector2 backPos2 = new Vector2(0,0);
    public Vector2 curBackPos = new Vector2(0,0);  
    public float backDirY = 1f;

   // public Vector2 titleOffset = new Vector2(1,1);


    private string roomName = "myRoom";

    private Vector2 scrollPos = Vector2.zero;

    private bool connectFailed = false;

    public static readonly string SceneNameMenu = "MainMenu";

    public static readonly string SceneNameGame = "GameScene";

    private string errorDialog;
    private double timeToClearDialog;

    public string ErrorDialog
    {
        get 
        { 
            return errorDialog; 
        }
        private set
        {
            errorDialog = value;
            if (!string.IsNullOrEmpty(value))
            {
                timeToClearDialog = Time.time + 4.0f;
            }
        }
    }

    public void Awake()
    {
        // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        PhotonNetwork.automaticallySyncScene = true;

        // the following line checks if this client was just created (and not yet online). if so, we connect
        if (PhotonNetwork.connectionStateDetailed == PeerState.PeerCreated)
        {
            // Connect to the photon master-server. We use the settings saved in PhotonServerSettings (a .asset file in this project)
            PhotonNetwork.ConnectUsingSettings("0.9");
        }

        // generate a name for this player, if none is assigned yet
        if (String.IsNullOrEmpty(PhotonNetwork.playerName))
        {
            PhotonNetwork.playerName = "Guest" + Random.Range(1, 9999);
        }

        // if you wanted more debug out, turn this on:
        // PhotonNetwork.logLevel = NetworkLogLevel.Full;

        startButtonPos1.x = Screen.width /2;
        startButtonPos1.y = Screen.height /2;
        startButtonPos2.x = Screen.width  * 1.2f;
        startButtonPos2.y = Screen.height /2 ; 
        startButtonCurPos.x = startButtonPos2.x;
        startButtonCurPos.y = startButtonPos2.y;



        exitButtonPos1.x = Screen.width /2;
        exitButtonPos1.y = Screen.height/2 * 1.5f;
        exitButtonPos2.x = Screen.width + Screen.width * -1.2f ;
        exitButtonPos2.y = Screen.height /2 * 1.1f; 
        exitButtonCurPos.x = exitButtonPos2.x;
        exitButtonCurPos.y = exitButtonPos2.y;

        servMenuPos1.x = Screen.width /2;
        servMenuPos1.y = Screen.height /2;
        servMenuPos2.x = Screen.width /2;
        servMenuPos2.y = Screen.height * -0.3f ;
        curServMenuPos.x = servMenuPos2.x ;
        curServMenuPos.y = servMenuPos2.y ; 

        titlePos1.x = Screen.width /2;
        titlePos1.y = Screen.height /2 * 1.4f;
        titlePos2.x = Screen.width /2;
        titlePos2.y = Screen.height * 1.4f  ;
        curTitlePos.x = titlePos2.x;
        curTitlePos.y = titlePos2.y;

        backPos1.x = Screen.width/2;
        backPos1.y = Screen.height * 0.8f;
        backPos2.x = Screen.width/2;
        backPos2.y = Screen.height * 1.2f;
        curBackPos.x = backPos2.x;
        curBackPos.y = backPos2.y;

        SetRectTransformPosition(imagePos, curTitlePos.x, curTitlePos.y);
    }



   
    void Update () {
        startButtonPos1.x = Screen.width /2;
        startButtonPos1.y = Screen.height /2;
        startButtonPos2.x = Screen.width  * 1.2f;
        startButtonPos2.y = Screen.height /2 ; 

        exitButtonPos1.x = Screen.width /2;
        exitButtonPos1.y = Screen.height/2 * 1.5f;
        exitButtonPos2.x = Screen.width + Screen.width * -1.2f ;;
        exitButtonPos2.y = Screen.height /2 * 1.1f; 
       
        servMenuPos1.x = Screen.width /2;
        servMenuPos1.y = Screen.height /2;
        servMenuPos2.x = Screen.width /2;
        servMenuPos2.y = Screen.height * -0.3f ;

        titlePos1.x = Screen.width /2;
        titlePos1.y = Screen.height /2 * 1.4f;
        titlePos2.y = Screen.height * 1.4f;

        backPos1.x = Screen.width/2;
        backPos1.y = Screen.height * 0.8f;
        backPos2.x = Screen.width/2;
        backPos2.y = Screen.height * 1.2f;
 
        curTitlePos.x = titlePos2.x;
        curServMenuPos.x = servMenuPos2.x;
        startButtonCurPos.y = startButtonPos2.y;
        exitButtonCurPos.y = exitButtonPos2.y;
        curBackPos.x = backPos2.x;



        if (showMenu){

            startButtonCurPos.x = Move(startButtonCurPos.x, startButtonPos1.x, startButtonDirX);
            exitButtonCurPos.x = Move(exitButtonCurPos.x, exitButtonPos1.x, exitButtonDirX);
            curTitlePos.y = Move(curTitlePos.y, titlePos1.y, servMenuDirY);

            SetRectTransformPosition(imagePos, curTitlePos.x + titleOffset.x, curTitlePos.y + titleOffset.y );
        }else {
            startButtonCurPos.x = Move(startButtonCurPos.x, startButtonPos2.x, -startButtonDirX);
            exitButtonCurPos.x = Move(exitButtonCurPos.x, exitButtonPos2.x, -exitButtonDirX);
            curTitlePos.y= Move(curTitlePos.y, titlePos2.y, -servMenuDirY);

            SetRectTransformPosition(imagePos, curTitlePos.x + titleOffset.x, curTitlePos.y + titleOffset.y );
        }



        if(showServerMenu){
             curServMenuPos.y = Move(curServMenuPos.y, servMenuPos1.y, -servMenuDirY);
             curBackPos.y = Move(curBackPos.y, backPos1.y, backDirY);
        } else {
            curServMenuPos.y = Move(curServMenuPos.y, servMenuPos2.y, servMenuDirY);
            curBackPos.y = Move(curBackPos.y, backPos2.y, -backDirY);

        }

    } // update

    public float Move(float _from, float to, float dir){

        if(!FastApproximately(_from, to)){
            float newCoordinate;

            newCoordinate = _from + menuAnimSpeed * Time.deltaTime * 100 * dir ;
            return newCoordinate;
        }else 
        return to;
    }

    public void SetRectTransformPosition(this RectTransform trans, float x, float y) {
            trans.position = new Vector3(x + (trans.pivot.x * trans.rect.width), y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
    }




    public void OnGUI()
    {
        if (this.Skin != null)
        {
            GUI.skin = this.Skin;
        }

        if (!PhotonNetwork.connected)
        {
            if (PhotonNetwork.connecting)
            {
                GUILayout.Label("Connecting to: " + PhotonNetwork.ServerAddress);
            }
            else
            {
                GUILayout.Label("Not connected. Check console output. Detailed connection state: " + PhotonNetwork.connectionStateDetailed + " Server: " + PhotonNetwork.ServerAddress);
            }
            
            if (this.connectFailed)
            {
                GUILayout.Label("Connection failed. Check setup and use Setup Wizard to fix configuration.");
                GUILayout.Label(String.Format("Server: {0}", new object[] {PhotonNetwork.ServerAddress}));
                GUILayout.Label("AppId: " + PhotonNetwork.PhotonServerSettings.AppID);
                
                if (GUILayout.Button("Try Again", GUILayout.Width(100)))
                {
                    this.connectFailed = false;
                    PhotonNetwork.ConnectUsingSettings("0.9");
                }
            }

            return;
        }




        if (GUI.Button(new Rect(startButtonCurPos.x-75, startButtonCurPos.y, 150, 30), "Start"))
        {
            showServerMenu = true;
            showMenu = false;
        }

		if (GUI.Button(new Rect(exitButtonCurPos.x-75, exitButtonCurPos.y, 150, 30), "Exit Game"))
		{
			Application.Quit();		
		}

        if (GUI.Button(new Rect(curBackPos.x-75, curBackPos.y, 150, 30), "Back"))
        {
            showServerMenu = false;
            showMenu = true;
        }
        



        Rect content = new Rect(curServMenuPos.x +(Screen.width - WidthAndHeight.x ) /2 + (servOffset.x*Screen.width), curServMenuPos.y + (Screen.height - WidthAndHeight.y) /2 + (servOffset.y * Screen.height), WidthAndHeight.x, WidthAndHeight.y);

        GUI.Box(content,"Join or Create Room");
        GUILayout.BeginArea(content);

        GUILayout.Space(40);
        
        // Player name
        //GUILayout.BeginHorizontal();
        //GUILayout.Label("Player name:", GUILayout.Width(150));
        //PhotonNetwork.playerName = GUILayout.TextField(PhotonNetwork.playerName);
        //GUILayout.Space(158);
        //if (GUI.changed)
        //{
            // Save name
        //    PlayerPrefs.SetString("playerName", PhotonNetwork.playerName);
       //}
        //GUILayout.EndHorizontal();

        GUILayout.Space(15);

        // Join room by title
        GUILayout.BeginHorizontal();
        GUILayout.Label("Roomname:", GUILayout.Width(150));
        this.roomName = GUILayout.TextField(this.roomName);
        
        if (GUILayout.Button("Create Room", GUILayout.Width(150)))
        {
            PhotonNetwork.CreateRoom(this.roomName, new RoomOptions() { maxPlayers = 10 }, null);
        }

        GUILayout.EndHorizontal();

        // Create a room (fails if exist!)
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        //this.roomName = GUILayout.TextField(this.roomName);
/*
        if (GUILayout.Button("Join Room", GUILayout.Width(150)))
        {
            PhotonNetwork.JoinRoom(this.roomName);
        }
*/
        GUILayout.EndHorizontal();


        if (!string.IsNullOrEmpty(this.ErrorDialog))
        {
            GUILayout.Label(this.ErrorDialog);

            if (timeToClearDialog < Time.time)
            {
                timeToClearDialog = 0;
                this.ErrorDialog = "";
            }
        }

        GUILayout.Space(25);

        // Join random room
        GUILayout.BeginHorizontal();

        GUILayout.Label(PhotonNetwork.countOfPlayers + " users are online in " + PhotonNetwork.countOfRooms + " rooms.");
        GUILayout.FlexibleSpace();

        GUILayout.EndHorizontal();

        GUILayout.Space(25);
        if (PhotonNetwork.GetRoomList().Length == 0)
        {
            GUILayout.Label("Currently no games are available.");
            //GUILayout.Label("Rooms will be listed here.");
        }
        else
        {
            GUILayout.Label(PhotonNetwork.GetRoomList().Length + " rooms available:");


            // Room listing: simply call GetRoomList: no need to fetch/poll whatever!
            this.scrollPos = GUILayout.BeginScrollView(this.scrollPos);
            foreach (RoomInfo roomInfo in PhotonNetwork.GetRoomList())
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(roomInfo.name + " " + roomInfo.playerCount + "/" + roomInfo.maxPlayers);
                if (GUILayout.Button("Join", GUILayout.Width(150)))
                {
                    PhotonNetwork.JoinRoom(roomInfo.name);
                }

                GUILayout.EndHorizontal();
            }

            GUILayout.EndScrollView();
        }

        GUILayout.EndArea();
    }

    public bool FastApproximately(float a, float b)
    {
         return ((a - b) < 0 ? ((a - b) * -1) : (a - b)) <= animThreshold;
    }    

    // We have two options here: we either joined(by title, list or random) or created a room.
    public void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
    }


    public void OnPhotonCreateRoomFailed()
    {
        this.ErrorDialog = "Error: Can't create room (room name maybe already used).";
        Debug.Log("OnPhotonCreateRoomFailed got called. This can happen if the room exists (even if not visible). Try another room name.");
    }

    public void OnPhotonJoinRoomFailed()
    {
        this.ErrorDialog = "Error: Can't join room (full or unknown room name).";
        Debug.Log("OnPhotonJoinRoomFailed got called. This can happen if the room is not existing or full or closed.");
    }
    public void OnPhotonRandomJoinFailed()
    {
        this.ErrorDialog = "Error: Can't join random room (none found).";
        Debug.Log("OnPhotonRandomJoinFailed got called. Happens if no room is available (or all full or invisible or closed). JoinrRandom filter-options can limit available rooms.");
    }

    public void OnCreatedRoom()
    {
        Debug.Log("OnCreatedRoom");
        PhotonNetwork.LoadLevel(SceneNameGame);
    }

    public void OnDisconnectedFromPhoton()
    {
        Debug.Log("Disconnected from Photon.");
    }

    public void OnFailedToConnectToPhoton(object parameters)
    {
        this.connectFailed = true;
        Debug.Log("OnFailedToConnectToPhoton. StatusCode: " + parameters + " ServerAddress: " + PhotonNetwork.networkingPeer.ServerAddress);
    }
}
