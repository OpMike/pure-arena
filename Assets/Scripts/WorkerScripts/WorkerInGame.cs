// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkerInGame.cs" company="Exit Games GmbH">
//   Part of: Photon Unity Networking
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;

public class WorkerInGame : Photon.MonoBehaviour
{	
	public GUISkin Skin;
  	public GUIStyle style = new GUIStyle();

    public Transform class1_Prefab;
	public Transform class2_Prefab;
	public Transform class3_Prefab;

	public int desiredTeamSize;
	public bool setDesiredTeamSize = false;
	public bool waitingForOthers = false;
	public float waitingForOthersOffset = 1f;


	public int team1_curSize = 0;
	public int team2_curSize = 0;
	public int team1_curDead = 0;
	public int team2_curDead = 0;
	public bool allowedToJoin_team1 = true;
	public bool allowedToJoin_team2 = true;
	public bool team1_allDead = false ;
	public bool team2_allDead = false;
	public bool fight = false;
	public bool playAgain = false;
	public GameObject[] allPlayers;


	public Transform team1_spawn;
	public Transform team2_spawn;
	private Transform spawn;

	private bool spawnNow = false; // spawn prefab now


	public bool setClass;
	public int classID;

	public bool setTeam;
	public int TeamID;

	public string choose;
	public string winMSG;
	//public bool playerIsReady = false;

	public bool showOptions = false;
	public float optionPos1;
	public float optionPos2;
	public float optionDirY;
	public float menuAnimSpeed;
	public float animThreshold;
	public float curOptionsPosY;
	
	public void Awake()
    {
        // in case we started this demo with the wrong scene being active, simply load the menu scene
        if (!PhotonNetwork.connected)
        {
            Application.LoadLevel(WorkerMenu.SceneNameMenu);
            return;
        }

		setClass = true;
		setTeam = true;
		classID = 0;
		TeamID = 0;
		curOptionsPosY = Screen.height/2 * optionPos2;

		if (PhotonNetwork.isMasterClient)
			setDesiredTeamSize = true;

		
        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
       // PhotonNetwork.Instantiate(this.playerPrefab.name, transform.position, Quaternion.identity, 0);
    }





	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		
		if (PhotonNetwork.isMasterClient){

			stream.SendNext(allowedToJoin_team1);
			stream.SendNext(allowedToJoin_team2);

			stream.SendNext(team1_allDead);
			stream.SendNext(team2_allDead);

			stream.SendNext(fight);
			stream.SendNext(waitingForOthers);

			stream.SendNext(playAgain);


			//stream.SendNext(targetViewID);

		}
		else{

			allowedToJoin_team1 = (bool)stream.ReceiveNext();
			allowedToJoin_team2 = (bool)stream.ReceiveNext();

			team1_allDead = (bool)stream.ReceiveNext();
			team2_allDead = (bool)stream.ReceiveNext();

			fight = (bool)stream.ReceiveNext();
			waitingForOthers = (bool)stream.ReceiveNext();

			playAgain = (bool)stream.ReceiveNext();

			//targetViewID = (int)stream.ReceiveNext();
		}
	}




	void Update () {

		if (PhotonNetwork.isMasterClient){

			GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
		
			if(players.Length >= 1){

				team1_curSize = 0;
				team2_curSize = 0;

				team1_curDead = 0;
				team2_curDead = 0;

				team1_allDead = false ;
				team2_allDead = false;

				foreach(GameObject pl in players){

					Player ply = ((Player)pl.GetComponent("Player"));
					if(ply != null){

							if ( ply.team == 1 ){	// Team 1

								team1_curSize++;

								if(ply.isDead)
									team1_curDead++;
								
							}
							if ( ply.team == 2 ){	// Team 2

								team2_curSize++;

								if(ply.isDead)
									team2_curDead++;
								
							}
					}
				}
			}

			if(team1_curSize > 0 && team2_curSize > 0){

				if (team1_curSize == team1_curDead)
					team1_allDead = true;

				if (team2_curSize == team2_curDead)
					team2_allDead = true;
			}


			if(team1_curSize < desiredTeamSize)
				allowedToJoin_team1 = true;
			else 
				allowedToJoin_team1 = false;

			if(team2_curSize < desiredTeamSize)
				allowedToJoin_team2 = true;
			else
				allowedToJoin_team2 = false;


			if(fight == true)
				allowedToJoin_team1  =  allowedToJoin_team2  =  false;





		} // If master client



		if( team1_allDead || team2_allDead  ){

			GameObject[] playrs = GameObject.FindGameObjectsWithTag("Player");
			if(playrs.Length != 0){
				
				
				foreach(GameObject pl in playrs){
					
					if ( ((Player)pl.GetComponent("Player")).mine == true){	
						
						if(team1_allDead){
							
							if(((Player)pl.GetComponent("Player")).team == 1)
								winMSG = "YOU LOSE!";
							if(((Player)pl.GetComponent("Player")).team == 2)
								winMSG = "YOU WIN!";
						}

						if(team2_allDead){
							if(((Player)pl.GetComponent("Player")).team == 2)
								winMSG = "YOU LOSE!";
							if(((Player)pl.GetComponent("Player")).team == 1)
								winMSG = "YOU WIN!";
						}
					}
					
				}// foreach
			}
		}



		GameObject[] doors = GameObject.FindGameObjectsWithTag("Door");

		if(doors != null){
			//Debug.Log(doors.Length);

			if (fight){

				foreach(GameObject go in doors){

					//go.SetActive(false); // doesnt work; GO.find => only active GOs
					go.collider.enabled = false;
					go.renderer.enabled = false;
				}

			}else{
			
				foreach(GameObject go in doors){
					
					//go.SetActive(true);
					go.collider.enabled = true;
					go.renderer.enabled = true;
				}
			}
		}

		SetOnlinePlayers();
	} // Update()



    public void OnGUI()
	{		

		//GUI.skin = null;
		if (this.Skin != null)
		{
			GUI.skin = this.Skin;
		}


		/// Player joining
		if(!allowedToJoin_team1 && !allowedToJoin_team2)
			choose = "Teams full";
		else
			choose = "CHOOSE A TEAM";
	
		if( setDesiredTeamSize ){
			GUI.BeginGroup (new Rect ((Screen.width/2)-150, (Screen.height/3)*2, 300,100));

				GUI.Box(new Rect(0,0, 300,100),"Select mode");
				
				GUI.BeginGroup (new Rect (35, 50, 220,30));

						if (GUI.Button(new Rect(5, 0, 100, 30), "1 v 1")){

								setDesiredTeamSize = false;
								desiredTeamSize = 1;
						}


						if (GUI.Button(new Rect(100+20, 0, 100, 30), "2 v 2")){

								setDesiredTeamSize = false;
								desiredTeamSize = 2;
						}
				GUI.EndGroup ();
			GUI.EndGroup ();
		}		

		  
		if( setTeam && !setDesiredTeamSize ){
			GUI.BeginGroup (new Rect ((Screen.width/2)-150, (Screen.height/3)*2, 300,100));

				GUI.Box(new Rect(0,0, 300,100),choose);
				
				GUI.BeginGroup (new Rect (35, 50, 220,30));
					if(allowedToJoin_team1){

						if (GUI.Button(new Rect(5, 0, 100, 30), "Team 1")){

								setTeam = false;
								TeamID = 1;
						}
					}

					if(allowedToJoin_team2){

						if (GUI.Button(new Rect(100+20, 0, 100, 30), "Team 2")){

								setTeam = false;
								TeamID = 2;
						}
					}

					if(!allowedToJoin_team1 && !allowedToJoin_team2){
						GUI.enabled = false;
						if (GUI.Button(new Rect(40, 0, 150, 30), "Spectate?")){}
						GUI.enabled = true;
					}

				GUI.EndGroup ();
			GUI.EndGroup ();
		}

		if(setClass && !setTeam){
			GUI.BeginGroup (new Rect ((Screen.width/2)-200, (Screen.height/3)*2, 400,100));

				GUI.Box(new Rect(0,0, 400,100),"CHOOSE A CLASS");
				GUI.BeginGroup (new Rect (50, 50, 350,30));
					if (GUI.Button(new Rect(0, 0, 150, 33), "Elementalist")){
						setClass = false;
						classID = 1;
					}
					
					if (GUI.Button(new Rect(150+10, 0, 150, 33), "Blood Priest")){
						setClass = false;
						classID = 2;
					} 
					/*
					GUI.enabled = false;
					if (GUI.Button(new Rect(300+20, 0, 150, 33), "Warrior")){
							Debug.Log("Warrior n/a");
						}
					GUI.enabled = true;
					*/
				GUI.EndGroup ();
			GUI.EndGroup ();


			spawnNow = true;

		}// setClass


		if(!setClass && !setTeam && spawnNow){

			if(TeamID == 1)
				spawn = team1_spawn;
			if(TeamID == 2)
				spawn = team2_spawn;

			if(classID == 1)
				PhotonNetwork.Instantiate(this.class1_Prefab.name, spawn.position,Quaternion.identity , 0);

				
			if(classID == 2)
				PhotonNetwork.Instantiate(this.class2_Prefab.name, spawn.position, Quaternion.identity, 0);
					
			spawnNow = false;	
			waitingForOthers = true;
		}

		if(waitingForOthers && !setClass && !setClass){
			style.alignment = TextAnchor.MiddleCenter;

			GUI.BeginGroup (new Rect ((Screen.width/2)-150, (Screen.height/3)*waitingForOthersOffset, 300,60));

				GUI.Box(new Rect(0,0, 300,60),"Waiting for more players and the room creator to start the match.", style);
				
			GUI.EndGroup ();
			style.alignment = TextAnchor.UpperCenter;
			

		}


		// Initiate Fight
		if(PhotonNetwork.isMasterClient){

			if (!allowedToJoin_team1 && !allowedToJoin_team2 && !fight){
				
				GUI.BeginGroup (new Rect ((Screen.width/2)-150, (Screen.height/2), 300,100));
					if (GUI.Button(new Rect(75, 0, 150, 30), "FIGHT!")){
						fight = true;
						waitingForOthers = false;
					}
				GUI.EndGroup ();

			}


			if (team2_allDead || team1_allDead){
				
				GUI.BeginGroup (new Rect ((Screen.width/2)-75, (Screen.height/2*0.7f), 300,100));
					if (GUI.Button(new Rect(0, 0, 150, 30), "Play again")){

					GetComponent<PhotonView>().RPC(
						"Respawn"
						, PhotonTargets.AllViaServer
						,new object[]{}		
					);

					fight = false;
					team2_allDead = team1_allDead = false;

					}
				GUI.EndGroup ();				
			}


		} // is Master client


		if(team2_allDead || team1_allDead){
			
			GUI.BeginGroup (new Rect ((Screen.width/2)-100, (Screen.height/4), 200,40));
			
				GUI.Box(new Rect(0,0, 200,40),winMSG);
			
			GUI.EndGroup ();
		}




		if (GUI.Button(new Rect(0, 0, 150, 30), "Options")){
			showOptions = !showOptions;
		}

		if(showOptions){
        	curOptionsPosY = Move(curOptionsPosY, Screen.height/2 * optionPos1, optionDirY);
		}else{
        	curOptionsPosY = Move(curOptionsPosY, Screen.height/2 * optionPos2, -optionDirY);

		}
			GUI.BeginGroup (new Rect (Screen.width/2-100, curOptionsPosY, 200,175));

				GUI.Box(new Rect(0,0, 200,175),"Options");

				if (GUI.Button(new Rect(25, 40, 150, 30), "Resume")){
					showOptions = !showOptions;
				}
				/*if (GUI.Button(new Rect(0, 40, 150, 30), "How to play")){

				}*/
				if (GUI.Button(new Rect(25, 80, 150, 30), "Leave Match")){
					PhotonNetwork.LeaveRoom(); 
				}
				if (GUI.Button(new Rect(25, 120, 150, 30), "Exit  Game")){
					Application.Quit();		
				}

			GUI.EndGroup ();	

	

	} // OnGUI

    public float Move(float _from, float to, float dir){

	    if(!FastApproximately(_from, to)){
	        float newCoordinate;

	        newCoordinate = _from + menuAnimSpeed * Time.deltaTime * 100 * dir ;
	        return newCoordinate;
	    }else 
	    return to;
    }

    public bool FastApproximately(float a, float b)
    {
         return ((a - b) < 0 ? ((a - b) * -1) : (a - b)) <= animThreshold;
    } 



	[RPC]
	public void Respawn(){

		GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
		
		foreach(GameObject pl in players){
						
			if(((Player)pl.GetComponent("Player"))){
				
				Player pla = ((Player)pl.GetComponent("Player"));
				
				if(pla.mine){
					
					pla.isDead = false;
					pla.curHealth = pla.playerClass.classHealth;

					if ( pla.team == 1 )	
						pl.transform.position = team1_spawn.position;

					if ( pla.team == 2 )	
						pl.transform.position = team2_spawn.position;
					
				}
			}
		}

	} // RPC Respawn

	private void SetOnlinePlayers(){
		if(!fight){
			if(GameObject.FindGameObjectsWithTag("Player") != null){
				allPlayers = GameObject.FindGameObjectsWithTag("Player");
			}
		}
	}


//////////////////////////////////////////////////////////////////////


    public void OnMasterClientSwitched(PhotonPlayer player)
    {
        Debug.Log("OnMasterClientSwitched: " + player);

        string message;
        InRoomChat chatComponent = GetComponent<InRoomChat>();  // if we find a InRoomChat component, we print out a short message

        if (chatComponent != null)
        {
            // to check if this client is the new master...
            if (player.isLocal)
            {
                message = "You are Master Client now.";
            }
            else
            {
                message = player.name + " is Master Client now.";
            }


            chatComponent.AddLine(message); // the Chat method is a RPC. as we don't want to send an RPC and neither create a PhotonMessageInfo, lets call AddLine()
        }
    }

    public void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom (local)");
        
        // back to main menu        
        Application.LoadLevel(WorkerMenu.SceneNameMenu);
    }

    public void OnDisconnectedFromPhoton()
    {
        Debug.Log("OnDisconnectedFromPhoton");

        // back to main menu        
        Application.LoadLevel(WorkerMenu.SceneNameMenu);
    }

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log("OnPhotonInstantiate " + info.sender);    // you could use this info to store this or react
    }

    public void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        Debug.Log("OnPhotonPlayerConnected: " + player);
    }

    public void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        Debug.Log("OnPlayerDisconneced: " + player);
    }

    public void OnFailedToConnectToPhoton()
    {
        Debug.Log("OnFailedToConnectToPhoton");

        // back to main menu        
        Application.LoadLevel(WorkerMenu.SceneNameMenu);
    }
}
