﻿using UnityEngine;
using System.Collections;

// Attatched to the Tidal Barrier prefab
public class Nova : MonoBehaviour {


	public float size;
	public float multiplier;
	public float radius = 5.0F;
	public float power = 100.0F;
	public ForceMode forceMode;

	// Simulates an explosion
	void Start () {
		size = 0.5f;
		multiplier = 1.04f;
		Destroy(this.gameObject,2.5f);

		Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

		foreach (Collider hit in colliders) {
			if (hit && hit.rigidbody)
				hit.rigidbody.AddExplosionForce(power, transform.position, radius,0.0f, forceMode);
			
		}
	}


	// Lets the explosion (collider & model) gro in size
	void Update () {

		if(size< 5f && GetComponent<SphereCollider>()  && GetComponent<ParticleSystem>() ){

			size+= 0.3f;

			SphereCollider sCol = (SphereCollider)GetComponent<SphereCollider>();
			ParticleSystem par =  (ParticleSystem)GetComponent<ParticleSystem>() ;

			par.startSize = size;

			sCol.radius = size/2;
		}
	}


	void OnCollisionEnter (Collision col){

	}
}
