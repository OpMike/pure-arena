﻿using UnityEngine;
using System.Collections;

public class CollisionDestroy : MonoBehaviour {


	public Buff debuff;
	public GameObject target;
	public string prefabName;
	public int moveSpeed;
	public int rotationSpeed;
	public float maxDistance;
	private Transform projTransform;
	public int damage;
	public float destructionTime;

	
	void Start () {
		projTransform = transform;
		maxDistance = 0;
	}
	
	void Update () {

		// If a target is present the projectile slowly follows it
		if (target != null) {

			// For debugging within Unity. Draws a line to the targeted player
			//Debug.DrawLine (target.transform.position, projTransform.position, Color.yellow);

			//look at target
			projTransform.rotation = Quaternion.Slerp (projTransform.rotation, Quaternion.LookRotation (target.transform.position - projTransform.position), rotationSpeed * Time.deltaTime);

			if (Vector3.Distance (target.transform.position, projTransform.position) > maxDistance) {
				//Move towards target
				projTransform.position += projTransform.forward * moveSpeed * Time.deltaTime;	
			}
		}

	}
	
	


	void OnCollisionEnter (Collision col)
	{ 	
		if(col.gameObject.GetComponent("Player")){

			Player pl = (Player)col.gameObject.GetComponent("Player");

			// If the player is shielded consumes the shield instead of damaging the player
			if(col.transform.Find("ShieldBuff(Clone)")){
				Destroy(col.transform.Find("ShieldBuff(Clone)").gameObject);
			}
			else{
				// Does the damage calculation locally and adds a debuff if there is one
				if(pl.mine)
					pl.addjustCurrentHealth(damage);

				if (debuff != null){
					debuff.Name = prefabName;
					pl.AddDebuff(debuff,target);
				}

				// instantiates the ability related collision prefab and assigns it as a child of the target 
				GameObject gol = (GameObject)Instantiate(Resources.Load("Prefabs/"+prefabName+"Col"));
				gol.transform.parent = col.gameObject.transform;
				gol.transform.localPosition = new Vector3(0f,0f,0);
				Destroy(gol,2.5f);
			}
		}

		// Adds the trail as child of the target for 0.25 sec, so the tail doesnt dissapear instantly
		if(this.transform.Find("trail")){

			Destroy(this.transform.Find("trail").gameObject,0.25f);

			GameObject trail = this.transform.Find("trail").gameObject;
			trail.transform.parent = col.transform;
		}

		Destroy(this.gameObject,destructionTime);

	}// OnCollisionEnter
}
